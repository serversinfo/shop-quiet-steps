#include <sdktools>
#include <shop>
#define CATEGORY	"stuff"
#define VERSION		"1.0.16"

bool g_bNinja[MAXPLAYERS+1];
ConVar g_hPrice;
ConVar g_hSellPrice;
ItemId g_id;

public Plugin myinfo =
{
	name	= "[Shop] Ninja footsteps",
	author	= "ShaRen",
	version	= VERSION
}


ConVar sv_footsteps;

public OnPluginStart()
{
	sv_footsteps = FindConVar("sv_footsteps");
	AddNormalSoundHook(Event_SoundPlayed);
	for(int i=1; i<MaxClients; i++)
		if (IsClientInGame(i))
			OnClientPutInServer(i);
	// SHOP
	g_hPrice = CreateConVar("sm_shop_footsteps_price", "40000", "Стоимость покупки кошачьих сапог.");
	HookConVarChange(g_hPrice, OnConVarChange);
	
	g_hSellPrice = CreateConVar("sm_shop_footsteps_sellprice", "35000", "Стоимость продажи кошачьих сапог.");
	HookConVarChange(g_hPrice, OnConVarChange);

	AutoExecConfig(true, "shop_quietsteps", "shop");
	
	if (Shop_IsStarted()) Shop_Started();
}

public OnConVarChange(ConVar hCvar, const char[] oldValue, const char[] newValue)
{
	if(g_id != INVALID_ITEM) {
		if(hCvar == g_hPrice) Shop_SetItemPrice(g_id, GetConVarInt(hCvar));
		else if(hCvar == g_hSellPrice) Shop_SetItemSellPrice(g_id, GetConVarInt(hCvar));
	}
}

public OnPluginEnd() Shop_UnregisterMe();

public void Shop_Started()
{
	CategoryId category_id = Shop_RegisterCategory(CATEGORY, "Дополнительно", "");
	
	if (Shop_StartItem(category_id, "quietsteps")) {
		Shop_SetInfo("[CT-Only] кошачьи сапоги", "Передвигайтесь бесшумно как ниндзя (только для КТ)", GetConVarInt(g_hPrice), GetConVarInt(g_hSellPrice), Item_Togglable, -1);
		Shop_SetCallbacks(OnItemRegistered, OnItemUsed);
		Shop_EndItem();
	}
}

public void OnItemRegistered(CategoryId category_id, const char[] category, const char[] item, ItemId item_id)
{
	g_id = item_id;
}

public ShopAction OnItemUsed(int iClient, CategoryId category_id, const char[] category, ItemId item_id, const char[] item, bool isOn, bool elapsed)
{
	if (isOn || elapsed) {
		g_bNinja[iClient] = false;
		return Shop_UseOff;
	}

	g_bNinja[iClient] = true;

	return Shop_UseOn;
}

public void OnClientPostAdminCheck(int iClient) 
{
	g_bNinja[iClient] = false;
}

public void OnClientPutInServer(int iClient)
{
	if(!IsFakeClient(iClient))
		SendConVarValue(iClient, sv_footsteps, "0");
}

public Action Event_SoundPlayed(int clients[64], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags) 
{
	if (0<entity<=MaxClients && sample[8] == 'f' && sample[9] == sample[10] && sample[11] == 't' && sample[22] != 's') {
		if(g_bNinja[entity] && GetClientTeam(entity) == 3)
			return Plugin_Handled;
	}
	return Plugin_Continue;
}

